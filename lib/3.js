"use strict";

var newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
};
var firstName = newObject.firstName,
    lastName = newObject.lastName,
    destination = newObject.destination,
    occupation = newObject.occupation; // Driver code

console.log(firstName, lastName, destination, occupation);