"use strict";

var newFunction = function newFunction(firstName, lastName) {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function fullName() {
      console.log(firstName + " " + lastName);
    }
  };
}; //Driver Code 


newFunction("William", "Imoh").fullName();